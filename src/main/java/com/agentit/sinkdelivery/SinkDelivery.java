/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agentit.sinkdelivery;

import com.cyclonecommerce.tradingengine.transport.FileNotFoundException;
import com.cyclonecommerce.tradingengine.transport.TransportInitializationException;
import com.cyclonecommerce.tradingengine.transport.TransportTestException;
import com.cyclonecommerce.tradingengine.transport.UnableToAuthenticateException;
import com.cyclonecommerce.tradingengine.transport.UnableToConnectException;
import com.cyclonecommerce.tradingengine.transport.UnableToConsumeException;
import com.cyclonecommerce.tradingengine.transport.UnableToDeleteException;
import com.cyclonecommerce.tradingengine.transport.UnableToDisconnectException;
import com.cyclonecommerce.tradingengine.transport.UnableToProduceException;
import com.cyclonecommerce.tradingengine.transport.pluggable.api.PluggableClient;
import com.cyclonecommerce.tradingengine.transport.pluggable.api.PluggableException;
import com.cyclonecommerce.tradingengine.transport.pluggable.api.PluggableMessage;
import com.cyclonecommerce.tradingengine.transport.pluggable.api.PluggableSettings;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Artturi
 */
public class SinkDelivery implements PluggableClient {
    
    /**
     * The log4j logger for this class
     */
    private static Logger logger = LogManager.getLogger(SinkDelivery.class.getName());

    
     /**
     * This method initializes the delivery and receives the delivery settings
     * from B2Bi through the parameter PluggableSettings object.
     *
     * @param ps
     * @throws TransportInitializationException
     */
    
    @Override
    public void init(PluggableSettings ps) throws TransportInitializationException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("init method");
    }

    @Override
    public void connect() throws UnableToConnectException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("connect method");
    }

    @Override
    public void authenticate() throws UnableToAuthenticateException {
        logger.info("authenticate method");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isPollable() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("isPollable method");
        return false;
    }

    @Override
    public String[] list() throws UnableToConsumeException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("list method");
        return null;
    }

    @Override
    public PluggableMessage produce(PluggableMessage pm, PluggableMessage pm1) throws UnableToProduceException {
        logger.info("SinkDelivery produce (returns null)");
        return null;
    }

    @Override
    public PluggableMessage consume(PluggableMessage pm, String string) throws UnableToConsumeException, FileNotFoundException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("SinkDelivery consume (returns null)");
        return null;
    }

    @Override
    public void delete(String string) throws UnableToDeleteException, FileNotFoundException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("delete method");
    }

    @Override
    public String test() throws TransportTestException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void disconnect() throws UnableToDisconnectException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public String getUrl() throws PluggableException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        logger.info("getUrl Method");
        return null;
    }
    
}
