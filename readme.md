## SinkDelivery

A simple delivery that makes the message disappear I.E. This has the same functionality as the service AD_SINK. 


However messages going through AD_SINK end up in sentinel database with the State (MetadataProcessed) and without extension in the FileName Column
whereas with this delivery the messages have State (Delivered) and full FileName in the sentinel db/table. This is a requirement for CoreDashBoard Reporter
so that succesful delivery can be confirmed